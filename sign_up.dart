import 'package:flutter/material.dart';
import 'package:flutter_screen/main.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  DecorationImage _backgroundImage() {
    return DecorationImage(
      fit: BoxFit.cover,
      colorFilter:
          ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
      image: const AssetImage('assets/background.jpg'),
    );
  }

  Widget _header() {
    return const Text(
      'CREATE ACCOUNT',
      style: TextStyle(
          color: Colors.orange, fontWeight: FontWeight.w600, fontSize: 30),
    );
  }

  Widget _textField(String labelT) {
    return TextField(
      decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: labelT,
          filled: true,
          fillColor: Colors.white),
      keyboardType: TextInputType.emailAddress,
    );
  }

  Widget _hasAccount() {
    return const Text(
      'Already have Account',
      style: TextStyle(
        fontSize: 18,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    Widget _loginTextButton() {
      return TextButton(
          child: const Text('LOGIN', style: TextStyle(fontSize: 18)),
          onPressed: () {
            Navigator.pop(context);
          });
    }

    Widget _signUpElevatedButton() {
      return ElevatedButton(
        child: const Text('REGESTER'),
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const Dashboard()),
          );
        },
      );
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text('Sign Up'),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: _backgroundImage(),
          ),
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: SingleChildScrollView(
              child: SizedBox(
                width: targetWidth,
                child: Column(
                  children: <Widget>[
                    _header(),
                    const SizedBox(
                      height: 20.0,
                    ),
                    _textField('First Name'),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _textField('Surname'),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _textField('Phone Number'),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _textField('Password'),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _textField('Confirm Password'),
                    const SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(height: 50, child: _signUpElevatedButton()),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _hasAccount(),
                        _loginTextButton(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
