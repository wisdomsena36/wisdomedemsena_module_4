import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

import './sign_up.dart';
import './feature_screens.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey)
                .copyWith(secondary: Colors.orange)),
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
          splash: 'assets/vitechsclogo.png',
          splashIconSize: 550,
          nextScreen: const LoginScreen(),
          splashTransition: SplashTransition.rotationTransition,
          backgroundColor: const Color.fromARGB(100, 52, 73, 85),
        ));
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginScreen();
  }
}

class _LoginScreen extends State<LoginScreen> {
  DecorationImage _backgroundImage() {
    return DecorationImage(
      fit: BoxFit.cover,
      colorFilter:
          ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
      image: const AssetImage('assets/background.jpg'),
    );
  }

  Widget _header() {
    return const Text(
      'VITECHSC',
      style: TextStyle(
          color: Colors.orange, fontWeight: FontWeight.w600, fontSize: 30),
    );
  }

  Widget _emailTextField() {
    return TextField(
      decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Username/email',
          filled: true,
          fillColor: Colors.white),
      keyboardType: TextInputType.emailAddress,
      onChanged: (String value) {},
    );
  }

  Widget _passwordTextField() {
    return const TextField(
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Password',
          filled: true,
          fillColor: Colors.white),
      obscureText: true,
    );
  }

  Widget _forgetPassword() {
    return TextButton(
      onPressed: () {
        //forgot password screen
      },
      child: const Text('Forgot Password', style: TextStyle(fontSize: 18)),
    );
  }

  Widget _noAccount() {
    return const Text(
      'Does not have account?',
      style: TextStyle(
        fontSize: 18,
      ),
    );
  }

  Widget _signUpTextButton() {
    return TextButton(
        child: const Text('REGESTER', style: TextStyle(fontSize: 18)),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const SignUpScreen()),
          );
        });
  }

  Widget _loginElevatedButton() {
    return ElevatedButton(
      child: const Text('LOGIN'),
      onPressed: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const Dashboard()),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    return Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
          automaticallyImplyLeading: false,
        ),
        body: Container(
          decoration: BoxDecoration(
            image: _backgroundImage(),
          ),
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: SingleChildScrollView(
              child: SizedBox(
                width: targetWidth,
                child: Column(
                  children: <Widget>[
                    _header(),
                    const SizedBox(
                      height: 20.0,
                    ),
                    _emailTextField(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _passwordTextField(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    _forgetPassword(),
                    const SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                        height: 50, width: 80, child: _loginElevatedButton()),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _noAccount(),
                        _signUpTextButton(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  static var myMenuItems = <String>[
    'Profile',
    'Logout',
  ];

  void onSelect(String item, BuildContext context) {
    switch (item) {
      case 'Profile':
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => const Profile()));
        break;
      case 'Logout':
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const LoginScreen()));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.95;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Dashboard"),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            PopupMenuButton<String>(
                onSelected: (item) => onSelect(item, context),
                itemBuilder: (BuildContext context) {
                  return myMenuItems.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                }),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const FloatButtonOption()));
          },
          child: const Icon(Icons.add),
        ),
        body: Center(
            child: SingleChildScrollView(
          child: Column(children: <Widget>[
            SizedBox(
              width: targetWidth,
              height: 50,
              child: ElevatedButton.icon(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const Courses()));
                },
                icon: const Icon(
                  Icons.library_books,
                ),
                label: const Text('Courses'),
              ),
            ),
            const SizedBox(height: 20),
            SizedBox(
                width: targetWidth,
                height: 50,
                child: ElevatedButton.icon(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const Forum()));
                  },
                  icon: const Icon(
                    Icons.forum,
                  ),
                  label: const Text('Forum'),
                )),
          ]),
        )));
  }
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Profile'),
        ),
        body: ListView(children: <Widget>[
          SizedBox(
            height: 250,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    CircleAvatar(
                      minRadius: 70.0,
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      child: const Text('WE',
                          style: TextStyle(
                              fontSize: 80,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  'Wisdom Edem',
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueGrey,
                  ),
                ),
              ],
            ),
          ),
          const Divider(
              color: Colors.grey,
              thickness: 0.5,
              indent: 12,
              endIndent: 12,
              height: 3),
          SizedBox(
            child: Column(
              children: const <Widget>[
                ListTile(
                  title: Text(
                    'Email',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                    ),
                  ),
                  subtitle: Text(
                    'techbusiness@tech.com',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Divider(
                    color: Colors.grey,
                    thickness: 0.5,
                    indent: 12,
                    endIndent: 12,
                    height: 3),
                ListTile(
                  title: Text(
                    'Phone',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                    ),
                  ),
                  subtitle: Text(
                    '+233 242145595',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Divider(
                    color: Colors.grey,
                    thickness: 0.5,
                    indent: 12,
                    endIndent: 12,
                    height: 3),
                ListTile(
                  title: Text(
                    'About',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                    ),
                  ),
                  subtitle: Text(
                    'Lorem ipsum dolor sit amet, ad quot dicant everti sed.'
                    'Eam tation perfecto id, eos ne eros volumus persecuti, '
                    'vis velit ludus phaedrum eu. Ad usu facer simul pertinacia,'
                    'vel ea unum tota noluisse. Dicta menandri lobortis at mei,'
                    'error oportere duo eu. In sit sale suscipit, qui ea quis'
                    'ludus persius.',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          )
        ]));
  }
}
