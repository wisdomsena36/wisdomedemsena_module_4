import 'package:flutter/material.dart';

class Courses extends StatelessWidget {
  const Courses({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Courses"),
        ),
        body: Center(
          child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                'Courses Feature Screen',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                ),
              )),
        ));
  }
}

class Forum extends StatelessWidget {
  const Forum({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Forum"),
        ),
        body: Center(
          child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                'Forum Feature Screen',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                ),
              )),
        ));
  }
}

class FloatButtonOption extends StatelessWidget {
  const FloatButtonOption({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Options"),
        ),
        body: Center(
          child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                'Float Button Screen',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                ),
              )),
        ));
  }
}
